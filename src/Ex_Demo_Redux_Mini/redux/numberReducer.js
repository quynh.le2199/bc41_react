let initialValue = { // giá trị mặc định của state khi user load trang lần đầu
    soLuong: 100,
}

export const numberReducer = (state=initialValue, action) => { //action là dữ liệu lấy từ dispatch trong hàm mapDispatchToProps gửi lên
    switch(action.type){ //bao nhiêu chức năng thì bấy nhiêu case
        case "TANG_SO_LUONG": {
            console.log("xử lý tăng số lượng");
            // state.soLuong++;
            // return {...state}
        }
        case "GIAM_SO_LUONG": {
            console.log("xử lý giảm số lượng");
            state.soLuong -= action.payload;
            return {...state}
        }
        default:
            return state
    }
}