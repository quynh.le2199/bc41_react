import { combineReducers } from "redux";
import { numberReducer } from "./numberReducer";

export const rootReducer_Ex_Demo_Reduce_Mini = combineReducers(
    {
        numberReducer: numberReducer, //lấy giá trị initialValue của numberReducer
    }
)

// key: quản lý reducer
// value: tên của reducer mà mình tạo ra