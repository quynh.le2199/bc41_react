import React, { Component } from 'react'
import {connect} from "react-redux";

class Ex_Demo_Redux_Mini extends Component {
  render() {
      console.log("props", this.props); //props ở đây gồm thuộc tính soLuong (dòng 19)
    return (
      <div>
        <button onClick={() => {this.props.handleGiamSoLuong(10)}} className='btn btn-danger'>-</button>
        <strong className='mx-5'>{this.props.soLuong}</strong>
        <button onClick={this.props.handleTangSoLuong()} className='btn btn-danger'>+</button>
      </div>
    )
  }
}

let mapStateToProps = (state) => {// lấy giá trị state của redux (là giá trị khi đã combineReducer trong file rootReducer: rootReducer_Ex_Demo_Reduce_Mini) về thành props của component
    return {
        soLuong: state.numberReducer.soLuong, //khi này soLuong sẽ là 1 thuộc tính của props cho component này (vd ở dòng 6)
    }
}
let mapDispatchToProps = (dispatch) => { //dispatch (còn gọi là action) gồm 2 thuộc tính là type (bắt buộc phải có để reducer biết đang làm hành động gì) và payload. từ dispatch sẽ gửi action lên reducer để xử lý
    return {
        handleTangSoLuong: () => {
            let action = {
                type: "TANG_SO_LUONG",
            };
            dispatch(action)
        },
        handleGiamSoLuong: () => {
            dispatch({
                type: "GIAM_SO_LUONG",
                payload: 10,
            })
        }
    }
}
// key: tên props (của component)
// value: giá trị muốn lấy từ state (của redux)

export default connect(mapStateToProps, mapDispatchToProps)(Ex_Demo_Redux_Mini);