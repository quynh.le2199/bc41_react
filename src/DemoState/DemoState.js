import React, { Component } from 'react'

export default class DemoState extends Component {
    // state: quản lý các giá trị ảnh hưởng tới việc update layout (hàm của react)
  state = {
    username: 'alice',
  };
  handleChangUsername = () => {
    let name;
    if (this.state.username == 'bob') {
        name = 'alice';
    }
    else {
        name = 'bob';
    }
    //this.setState(): sinh ra để update giá trị của state
    this.setState({
        username: name,
    })
  }
  render() {
    return ( // trong return ko dùng đc if-else mà phải dùng toán tử 3 ngôi (condition ? value 1 : value 2)
      <div>
        <h2>DemoState</h2>
        <h3 className={this.state.username == 'bob' ? 'text-danger' : 'text-primary'}>{this.state.username}</h3>
        <button onClick={this.handleChangUsername} className='btn btn-warning'>Change username</button>
      </div>
    )
  }
}
