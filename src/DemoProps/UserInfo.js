import React, { Component } from 'react'

export default class UserInfo extends Component {
  render() {
    // tất cả dữ liệu truyền từ bên ngoài vào (component cha) đều dùng cú pháp props
    console.log('props',this.props);
    return (
      <div>
        <h2>UserInfo</h2>
        <h3>Username: {this.props.hoTen}</h3>
        <h3>Age: {this.props.tuoi}</h3>
        <button onClick={this.props.handleOnclick} className='btn btn-success'>Change username</button>
      </div>
    )
  }
}
