import React, { Component } from 'react'
import UserInfo from './UserInfo'

export default class DemoProps extends Component {
    // mỗi component có 1 state riêng; tùy lúc mình cần dùng vào mục dịch gì thì tạo state trong component đó
    state = {
        username: 'alice',
    }
    handleChangeUsername = () => {
        this.setState({
            username: 'bob'
        })
    }
  render() {
    return (
      <div>
        <h2>DemoProps</h2>
        <UserInfo hoTen = {this.state.username} tuoi="20" handleOnclick = {this.handleChangeUsername}/>
      </div>
    )
  }
}
// component cha
// component con
