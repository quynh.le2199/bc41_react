import React, { Component } from 'react'
import DetailPhone from './DetailPhone'
import { data_phone } from './data_phone'
import ListPhone from './ListPhone'

export default class Ex_Phone extends Component {
    state = {
        listPhone: data_phone,
        detail: data_phone[0],
    }
  render() {
    return (
      <div className='container'>
        <ListPhone list={this.state.listPhone}/>
        <DetailPhone detail={this.state.detail}/>
      </div>
    )
  }
}
