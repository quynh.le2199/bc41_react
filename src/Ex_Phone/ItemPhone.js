import React, { Component } from 'react'

export default class ItemPhone extends Component {
  render() {
    let {tenSP, giaBan, hinhAnh} = this.props.phone;
    return (
      <div className='col-4 p-4'>
        <div className="card border-primary">
            <img src={hinhAnh} alt />
            <div className="card-body">
                <h4 className="card-title">{tenSP}</h4>
                <p className="card-text">{giaBan}</p>
                <button className='btn btn-primary'>Xem chi tiết</button>
            </div>
        </div>
      </div>
    )
  }
}
