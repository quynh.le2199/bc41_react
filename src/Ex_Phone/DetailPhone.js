import React, { Component } from 'react'

export default class DetailPhone extends Component {
  render() {
    let {maSP, tenSP, manHinh, heDieuHanh, cameraTruoc, cameraSau, ram, rom, giaBan, hinhAnh} = this.props.detail;
    return (
      <div className='row border border-primary mb-5 py-4'>
        <div className="col-6">
            <img src={hinhAnh} alt="" style={{width:'100%'}}/>
        </div>
        <div className="col-6 text-left">
            <h2>{tenSP}</h2>
            <p>Mã sản phẩm: {maSP}</p>
            <p>Màn hình: {manHinh}</p>
            <p>Hệ điều hành: {heDieuHanh}</p>
            <p>Camera trước: {cameraTruoc}</p>
            <p>Camera sau: {cameraSau}</p>
            <p>RAM: {ram}</p>
            <p>ROM: {rom}</p>
            <p>Giá bán: {giaBan}</p>
        </div>
      </div>
    )
  }
}
