import React, { Component } from 'react'

export default class ConditionalRendering extends Component {
  isLogin = false; //dùng dấu bằng = để khai báo nha!, dấu 2 chấm : chỉ dùng trong object
  handleDangNhap = () => {
    console.log('before', this.isLogin);
    this.isLogin = true;
    console.log('after', this.isLogin);
  }
  handleDangXuat = () => {
    console.log('before', this.isLogin);
    this.isLogin = false;
    console.log('after', this.isLogin);
  }

  renderContentButton = () => {
    if (this.isLogin) {
        // TH đã đăng nhập 
        return <button onClick={this.handleDangXuat} className='btn btn-danger'>Log out</button>;
    }
    else {
        return <button onClick={this.handleDangNhap} className='btn btn-success'>Log In</button>;
    }
  }
  render() {
    return (
      <div>
        <h2>ConditionalRendering</h2>
        <div>
            {this.renderContentButton()} 
        </div>
      </div>
    )
  }
}
