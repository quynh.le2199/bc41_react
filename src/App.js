import './App.css';
import DemoClass from './DemoComponent/DemoClass';
import DemoFunction from './DemoComponent/DemoFunction';
import Ex_Layout from './Ex_Layout/Ex_Layout';
import DataBinding from './DataBinding/DataBinding';
import EventHandling from './EventHandling/EventHandling';
import ConditionalRendering from './ConditionalRendering/ConditionalRendering';
import DemoState from './DemoState/DemoState';
import RenderWithMap from './RenderWithMap/RenderWithMap';
import Ex_Car_Color from './Ex_Car_Color/Ex_Car_Color';
import DemoProps from './DemoProps/DemoProps';
import Ex_Shoe_Shop from './Ex_Shoe_Shop/Ex_Shoe_Shop';
import Ex_Phone from './Ex_Phone/Ex_Phone';
import Ex_Demo_Redux_Mini from './Ex_Demo_Redux_Mini/Ex_Demo_Redux_Mini';
import Ex_Shoe_Shop_Redux from './Ex_Shoe_Shop_Redux/Ex_Shoe_Shop_Redux';

function App() {
  return (
    <div className="App">
      {/* Buổi 1 */}
      {/* <DemoClass/> */}
      {/* <DemoClass/> */}
      {/* <DemoFunction></DemoFunction> */}
      {/* <Ex_Layout/> */}
      {/* <DataBinding/> */}

      {/* Buổi 2 */}
      {/* <EventHandling/> */}
      {/* <ConditionalRendering/> */}
      {/* <DemoState/> */}
      {/* <RenderWithMap/> */}
      {/* <Ex_Car_Color/> */}

      {/* Buổi 3 */}
      {/* <DemoProps/> */}
      {/* <Ex_Shoe_Shop/> */}
      {/* <Ex_Phone/> */}

      {/* Redux */}
      {/* <Ex_Demo_Redux_Mini/> */}
      <Ex_Shoe_Shop_Redux/>
    </div>
  );
}

export default App;
