import React, { Component } from 'react'
import { data_shoe } from './data_shoe'
import ListShoe from './ListShoe'
import CartShoe from './CartShoe'

export default class Ex_Shoe_Shop_Redux extends Component {
    state = {
        listShoe: data_shoe,
        cart: [],
    }
    handleChangeQuantity = (idShoe, luaChon) => {
      // luaChon: 1 hoặc -1
      let cloneCart = [...this.state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id == idShoe;
      })
      cloneCart[index].soLuong += luaChon;
      this.setState({cart: cloneCart})
    }
    handleAddToCart = (shoe) => { //đc truyền tham số là object
      // TH1: chưa có trong giỏ hàng => push
      // TH2: đã có trong giỏ hàng => update key số lượng
      
      // clone cart ra 1 array mới chứ ko sửa trực tiếp các key trong state
      let cloneCart = [...this.state.cart]
      let index = cloneCart.findIndex((item) => { //tìm item shoe trong cloneCart
        return item.id == shoe.id;
      })
      if (index == -1){ //TH1
        let newShoe ={...shoe, soLuong: 1};
        cloneCart.push(newShoe);
      }
      else { //TH2
        cloneCart[index].soLuong++;
      }
      this.setState({
        cart: cloneCart,
      })
    }
    handleDelete = (idShoe) => {
      // thay vì xóa bằng splice(), mình sẽ tạo mảng mới từ mảng cũ mà phần tử có id khác với idShoe
      let newCart = this.state.cart.filter((item) => {
        return item.id != idShoe
      })
      this.setState({cart: newCart})
    }
  render() {
    return (
      <div className='container'>
        <h2>Ex_Shoe_Shop</h2>
        <div className="row">
          <div className="col-8">
            <CartShoe handleChangeQuantity={this.handleChangeQuantity} handleDelete={this.handleDelete}/>
          </div>
          <div className="col-4">
            <ListShoe/> 
          </div>
        </div>
      </div>
    )
  }
}
