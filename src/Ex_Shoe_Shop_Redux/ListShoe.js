import React, { Component } from 'react'
import ItemShoe from './ItemShoe'
import { data_shoe } from './data_shoe'
import { connect } from 'react-redux'

class ListShoe extends Component {
  render() {
    return (
      <div>
        <h2>ListShoe</h2>
        <div className='row'>
            {this.props.list.map((item) => {
                return <ItemShoe shoe={item}/>
            })}
        </div>
      </div>
    )
  }
}
let mapStateToProps = (state) => {
  return {
    list: state.shoeReducer.listShoe,
  }
}
export default connect(mapStateToProps)(ListShoe); //dòng này có 2 function, khi connect chạy xong sẽ trả ra 1 function, function đó sẽ nhận Listshoe làm tham số 
